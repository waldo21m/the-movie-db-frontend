import {createStyles, makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      position: 'fixed',
      bottom: theme.spacing(2),
      right: theme.spacing(2),
    },
    popularMovieLogo: {
      width: '40px',
    },
    popularMovieAppBar: {
      fontWeight: 500,
    }
  }),
);

export default useStyles;
