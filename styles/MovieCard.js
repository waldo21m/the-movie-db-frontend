import {createStyles, makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      maxWidth: 345,
      width: '100%',
    },
    media: {
      backgroundPosition: 'center top',
      height: 400,
    },
  }),
);

export default useStyles;
