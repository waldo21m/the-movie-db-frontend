import {createStyles, makeStyles} from '@material-ui/core/styles';
import Banner from '../public/images/john-wick-parabellum.jpg';

const useStyles = makeStyles((theme) =>
  createStyles({
    banner: {
      backgroundRepeat: 'no-repeat',
      backgroundSize: 'cover',
      backgroundPosition: 'center top',
      opacity: 0.9,
      width: '100%',
      height: '500px',
      marginBottom: theme.spacing(4),
      '@media (max-width:599px)': {
        display: 'none',
      },
      '@media (min-width:1800px)': {
        height: '600px',
      },
    },
    container: {
      '@media (max-width:599px)': {
        marginTop: theme.spacing(2),
      },
    },
    posterGrid: {
      display: 'flex',
      justifyContent: 'center',
    },
    movieDetailsGrid: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'space-between',
    },
    originalTitle: {
      fontWeight: 500,
      marginRight: theme.spacing(2),
    },
  }),
);

export default useStyles;
