import {createStyles, makeStyles} from '@material-ui/core/styles';
import Banner from '../public/images/john-wick-parabellum.jpg';

const useStyles = makeStyles((theme) =>
  createStyles({
    banner: {
      backgroundImage: `url(${Banner})`,
      backgroundRepeat: 'no-repeat',
      backgroundSize: 'cover',
      backgroundPosition: 'center top',
      opacity: 0.9,
      width: '100%',
      height: '500px',
      marginBottom: theme.spacing(4),
      '@media (min-width:1800px)': {
        height: '600px',
      },
    },
    title: {
      fontWeight: 500,
      marginBottom: theme.spacing(4),
    },
    gridItem: {
      display: 'flex',
      justifyContent: 'center',
    }
  }),
);

export default useStyles;
