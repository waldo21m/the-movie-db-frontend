# 🎬 The Movie DB Frontend

The Movie DB es una aplicación para ver las películas más populares.

## 💻 Índice

* [Arquitectura del la aplicación](#archApp)
    * [Librerías implementadas para complementar el desarrollo del Frontend](#libraries)
* [Pasos iniciales](#initApp)
* [Ejecutar la aplicación](#execApp)
* [Y mas...](#more)

## <a name="archApp"></a> 🚀 Arquitectura del la aplicación

La aplicación está construida en [NextJs](https://nextjs.org/) (Framework de ReactJs) usando [GraphQL](https://graphql.org/).

### <a name="libraries"> 📚 Librerías implementadas para complementar el desarrollo del Backend
Podemos destacar algunas de las librerías:
* [Material-UI](https://material-ui.com/es/)
* [Redux](https://es.redux.js.org/)
* [Graphql](https://graphql.org/)
* [Apollo Client](https://www.apollographql.com/docs/react/)
* [Next](https://nextjs.org/)

*Nota: Para mayor información del resto de las dependencias, no dude en consultar el archivo package.json.*

## <a name="initApp"></a> 🐾 Pasos iniciales
Una vez clonado el proyecto, debemos movernos a la carpeta raíz, ejecutando el siguiente comando:

```sh
cd the-movie-db-frontend
```

Luego necesitamos ejecutar el siguiente comando para instalar todas las librerías necesarias:

```sh
npm i
```

## <a name="execApp"></a> 🤓 Ejecutar la aplicación
Para iniciar la aplicación ejecutamos el siguiente comando:

```sh
npm run dev
```

## <a name="more"></a> 💡 Y mas...
Por fines prácticos, los archivos de configuración fueron subidos al repositorio y no son un mero accidente.