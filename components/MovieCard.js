import React from 'react';
import useStyles from '../styles/MovieCard';
import Link from 'next/link';
import {
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  Typography,
} from '@material-ui/core';
import Rating from '@material-ui/lab/Rating';

const MovieCard = ({movieId, posterPath, originalTitle, voteAverage}) => {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <Link href={`/${movieId}`}>
        <CardActionArea>
          <CardMedia
            className={classes.media}
            image={`https://image.tmdb.org/t/p/w300${posterPath}`}
            title={originalTitle}
          />
          <CardContent>
            <Typography gutterBottom variant='h5' component='h2'>
              {originalTitle}
            </Typography>
            <Typography variant='body2' align='center' color='textSecondary' component='p'>
              <Rating defaultValue={voteAverage} precision={0.1} readOnly max={10} />
            </Typography>
          </CardContent>
        </CardActionArea>
      </Link>
    </Card>
  );
};

export default MovieCard;
