import React from 'react';
import useStyles from '../styles/PosterCard';
import Link from 'next/link';
import {
  Card,
  CardActionArea,
  CardMedia,
} from '@material-ui/core';

const PosterCard = ({posterPath, originalTitle}) => {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <Link href={`https://image.tmdb.org/t/p/original/${posterPath}`}>
        <CardActionArea>
          <CardMedia
            className={classes.media}
            image={`https://image.tmdb.org/t/p/w300${posterPath}`}
            title={originalTitle}
          />
        </CardActionArea>
      </Link>
    </Card>
  );
};

export default PosterCard;
