import React from 'react';
import Link from 'next/link';
import PropTypes from 'prop-types';
import useStyles from '../styles/Layout';
import {
  AppBar,
  Box,
  CssBaseline,
  Fab,
  useScrollTrigger,
  Toolbar,
  Typography,
  Zoom,
} from '@material-ui/core';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import PopularMovieLogo from '../public/images/clapperboard.svg';

const ScrollTop = (props) => {
  const {children, window} = props;
  const classes = useStyles();
  // Note that you normally won't need to set the window ref as useScrollTrigger
  // will default to window.
  // This is only being set here because the demo is in an iframe.
  const trigger = useScrollTrigger({
    target: window ? window() : undefined,
    disableHysteresis: true,
    threshold: 100,
  });

  const handleClick = (event) => {
    const anchor = (event.target.ownerDocument || document).querySelector('#back-to-top-anchor');

    if (anchor) {
      anchor.scrollIntoView({behavior: 'smooth', block: 'center'});
    }
  };

  return (
    <Zoom in={trigger}>
      <div onClick={handleClick} role='presentation' className={classes.root}>
        {children}
      </div>
    </Zoom>
  );
};

ScrollTop.propTypes = {
  children: PropTypes.element.isRequired,
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window: PropTypes.func,
};

const Layout = (props) => {
  const classes = useStyles();

  return (
    <>
      <CssBaseline/>
      <AppBar>
        <Toolbar>
          <Link href='/'>
            <Box style={{cursor: 'pointer'}} display='flex' alignItems='center' flexWrap='wrap'>
              <Box mr={2}>
                <img src={PopularMovieLogo} alt='Popular Movie Logo' className={classes.popularMovieLogo}/>
              </Box>
              <Typography variant='h4' className={classes.popularMovieAppBar}>Popular Movies</Typography>
            </Box>
          </Link>
        </Toolbar>
      </AppBar>
      <Toolbar id='back-to-top-anchor'/>
      {props.children}
      <ScrollTop {...props}>
        <Fab color='secondary' size='small' aria-label='scroll back to top'>
          <KeyboardArrowUpIcon/>
        </Fab>
      </ScrollTop>
    </>
  );
};

export default Layout;
