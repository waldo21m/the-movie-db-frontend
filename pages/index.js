import React, {useEffect, useState} from 'react';
import useStyles from '../styles/home';
import {useDispatch, useSelector} from 'react-redux';
import Layout from '../components/Layout';
import Head from 'next/head';
import {
  Box,
  CircularProgress,
  Container,
  Grid,
  Snackbar,
  Typography,
} from '@material-ui/core';
import MovieCard from '../components/MovieCard';
import MuiAlert from '@material-ui/lab/Alert';
import {MOVIES_LIST_CLEAR_ERROR} from '../types';
import {fetchMoviesList} from '../actions/movieActions';
import Pagination from '@material-ui/lab/Pagination';

const Home = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.movie.loading);
  const moviesList = useSelector((state) => state.movie.moviesList);
  const error = useSelector((state) => state.movie.error);
  const errorMessage = useSelector((state) => state.movie.errorMessage);
  const [page, setPage] = useState(1);

  const Loader = () => {
    return (
      <Grid item xs={12} className={classes.gridItem}>
        <CircularProgress />
      </Grid>
    );
  };

  useEffect(() => {
    dispatch(fetchMoviesList(page));
  }, [page]);

  const handleErrorClose = () => {
    dispatch({
      type: MOVIES_LIST_CLEAR_ERROR,
    });
  };

  const handleChange = (event, value) => {
    setPage(value);
  };

  return (
    <Layout>
      <Head>
        <title>Popular Movies | Movies list</title>
        <link rel='icon' href='/favicon.ico'/>
      </Head>

      <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        open={error}
        autoHideDuration={3000}
        onClose={handleErrorClose}
      >
        <MuiAlert onClose={handleErrorClose} severity='error'>
          {errorMessage}
        </MuiAlert>
      </Snackbar>

      <Box className={classes.banner}/>

      <Container>
        <Typography
          variant='h1'
          align='center'
          style={{fontWeight: 500, marginBottom: '32px'}}
        >
          Popular Movies
        </Typography>

        <Grid container spacing={4}>
          {!loading ? (
            <>
              <Grid item xs={12} className={classes.gridItem}>
                <Pagination
                  count={moviesList.totalPages}
                  defaultPage={page}
                  color='secondary'
                  size='large'
                  showFirstButton
                  showLastButton
                  onChange={handleChange}
                />
              </Grid>

              {moviesList.items.map((movie, index) => {
                return (
                  <Grid item xs={12} sm={6} lg={3} className={classes.gridItem} key={index}>
                    <MovieCard
                      movieId={movie.id}
                      posterPath={movie.posterPath}
                      originalTitle={movie.originalTitle}
                      voteAverage={movie.voteAverage}
                    />
                  </Grid>
                );
              })}

              <Grid item xs={12} className={classes.gridItem}>
                <Pagination
                  count={moviesList.totalPages}
                  defaultPage={page}
                  color='secondary'
                  size='large'
                  showFirstButton
                  showLastButton
                  onChange={handleChange}
                />
              </Grid>
            </>
          ) : <Loader/>}
        </Grid>
      </Container>
    </Layout>
  )
};

export default Home;
