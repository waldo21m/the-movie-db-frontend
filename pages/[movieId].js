import React, {useEffect} from 'react';
import useStyles from '../styles/movie-details';
import {useRouter} from 'next/router';
import {useDispatch, useSelector} from 'react-redux';
import Layout from '../components/Layout';
import Head from 'next/head';
import {
  Box,
  Button,
  CircularProgress,
  Container,
  Grid,
  Snackbar,
  Typography,
} from '@material-ui/core';
import MuiAlert from '@material-ui/lab/Alert';
import {MOVIE_CLEAR_ERROR} from '../types';
import Rating from '@material-ui/lab/Rating';
import PosterCard from '../components/PosterCard';
import Link from 'next/link';
import {fetchMovie} from "../actions/movieActions";

const MovieDetails = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const router = useRouter();
  const loading = useSelector((state) => state.movieDetails.loading);
  const movie = useSelector((state) => state.movieDetails.movie);
  const error = useSelector((state) => state.movieDetails.error);
  const errorMessage = useSelector((state) => state.movieDetails.errorMessage);
  const {movieId} = router.query;

  useEffect(() => {
    if (movieId) {
      dispatch(fetchMovie(movieId));
    }
  }, [movieId]);

  const handleErrorClose = () => {
    dispatch({
      type: MOVIE_CLEAR_ERROR,
    });
  };

  return (
    <Layout>
      <Head>
        <title>Popular Movies | Movie details</title>
        <link rel='icon' href='/favicon.ico'/>
      </Head>

      <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        open={error}
        autoHideDuration={3000}
        onClose={handleErrorClose}
      >
        <MuiAlert onClose={handleErrorClose} severity='error'>
          {errorMessage}
        </MuiAlert>
      </Snackbar>

      {!loading ? (
        <>
          <Box className={classes.banner} style={{
            backgroundImage: `url(https://image.tmdb.org/t/p/original/${movie.backdropPath})`
          }}/>

          <Container className={classes.container}>
            <Grid container spacing={4}>
              <Grid item xs={12} sm={4} className={classes.posterGrid}>
                <PosterCard
                  posterPath={movie.posterPath}
                  originalTitle={movie.originalTitle}
                />
              </Grid>
              <Grid item xs={12} sm={8} className={classes.movieDetailsGrid}>
                <Box mb={2}>
                  <Box mb={1}>
                    <Typography
                      variant='h1'
                      style={{
                        fontWeight: 500,
                        marginRight: '16px',
                      }}
                      gutterBottom
                      display='inline'
                    >
                      {movie.originalTitle}
                    </Typography>
                    <Typography
                      variant='h2'
                      gutterBottom
                      display='inline'
                    >
                      ({new Date(movie.releaseDate).getFullYear()})
                    </Typography>
                  </Box>
                  <Typography variant='body2' color='textSecondary' gutterBottom>
                    <Rating defaultValue={movie.voteAverage} precision={0.1} readOnly max={10} />
                  </Typography>
                  <Typography variant='h3' color='textSecondary' gutterBottom>
                    {movie.tagline}
                  </Typography>
                  <Typography variant='body1' color='textSecondary'>
                    {movie.overview}
                  </Typography>
                </Box>
                <Box display='flex' justifyContent='center'>
                  <Link href='/'>
                    <Button
                      variant='contained'
                      color='primary'
                      fullWidth
                    >
                      Go Back
                    </Button>
                  </Link>
                </Box>
              </Grid>
            </Grid>
          </Container>
        </>
      ) : (
        <Box mt={4} display='flex' justifyContent='center' alignItems='center'>
          <CircularProgress/>
        </Box>
      )}
    </Layout>
  )
};

export default MovieDetails;
