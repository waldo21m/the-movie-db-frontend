/**
 * Types are used in the actions and reducers and described what happen in
 * the app
 */
export const MOVIES_LIST_LOAD = 'MOVIES_LIST_LOAD';
export const MOVIES_LIST_SUCCESS = 'MOVIES_LIST_SUCCESS';
export const MOVIES_LIST_CLEAR_ERROR = 'MOVIES_LIST_CLEAR_ERROR';
export const MOVIES_LIST_ERROR = 'MOVIES_LIST_ERROR';

export const MOVIE_LOAD = 'MOVIE_LOAD';
export const MOVIE_SUCCESS = 'MOVIE_SUCCESS';
export const MOVIE_CLEAR_ERROR = 'MOVIE_CLEAR_ERROR';
export const MOVIE_ERROR = 'MOVIE_ERROR';
