import {
  MOVIES_LIST_LOAD,
  MOVIES_LIST_SUCCESS,
  MOVIES_LIST_ERROR,
  MOVIE_LOAD,
  MOVIE_SUCCESS,
  MOVIE_ERROR,
} from '../types';
import clientApollo from '../config/apollo';
import {MOVIES_LIST_QUERY, MOVIE_QUERY} from './movieQueries';

/**
 * Fetch movies list data.
 *
 * @param page - Page for the paginator.
 *
 * @returns The movies list paginated.
 */
export function fetchMoviesList(page = 1) {
  return async (dispatch) => {
    dispatch({
      type: MOVIES_LIST_LOAD,
    });

    const variables = {
      page,
    };

    try {
      const response = await clientApollo.query({
        query: MOVIES_LIST_QUERY,
        variables,
      });

      dispatch({
        type: MOVIES_LIST_SUCCESS,
        payload: response.data.moviesList,
      });
    } catch (error) {
      console.log('Fetch movies list error: ', error);
      dispatch({
        type: MOVIES_LIST_ERROR,
        payload: error.toString()
      });
    }
  }
}

/**
 * Fetch movie data.
 *
 * @param movieId - Movie id for the search.
 *
 * @returns The movie details.
 */
export function fetchMovie(movieId = 1) {
  return async (dispatch) => {
    dispatch({
      type: MOVIE_LOAD,
    });

    const variables = {
      movieId,
    };

    try {
      const response = await clientApollo.query({
        query: MOVIE_QUERY,
        variables,
      });

      dispatch({
        type: MOVIE_SUCCESS,
        payload: response.data.movie,
      });
    } catch (error) {
      console.log('Fetch movie error: ', error);
      dispatch({
        type: MOVIE_ERROR,
        payload: error.toString()
      });
    }
  }
}

