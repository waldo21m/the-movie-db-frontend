import {gql} from '@apollo/client';

export const MOVIES_LIST_QUERY = gql`
  query moviesList($page: Int) {
    moviesList(page: $page) {
      items {
        id
        originalTitle
        posterPath
        voteAverage
      }
      page
      totalPages
    }
  }
`;

export const MOVIE_QUERY = gql`
  query movie($movieId: ID!) {
    movie(id: $movieId) {
      backdropPath
      originalTitle
      overview
      posterPath
      releaseDate
      tagline
      voteAverage
    }
  }
`;
