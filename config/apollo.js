import {ApolloClient, createHttpLink, InMemoryCache} from '@apollo/client';
import fetch from 'node-fetch';
import {setContext} from 'apollo-link-context';

const httpLink = createHttpLink({
  uri: process.env.NEXT_PUBLIC_BASE_URL,
  fetch
});

const authLink = setContext((_, {headers}) => {
  // Get the authentication token from event storage if it exists.
  const token = localStorage.getItem('token');
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : ''
    }
  }
});

const client = new ApolloClient({
  connectToDevTools: true,
  cache: new InMemoryCache(),
  link: authLink.concat(httpLink)
});

export default client;
