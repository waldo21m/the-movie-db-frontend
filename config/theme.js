import {createMuiTheme} from '@material-ui/core/styles';
import {indigo, cyan} from '@material-ui/core/colors';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: indigo['500'],
    },
    secondary: {
      main: cyan['700'],
    },
  },
  typography: {
    h1: {
      fontSize: 24,
      '@media (min-width:600px)': {
        fontSize: 32,
      }
    },
    h2: {
      fontSize: 22,
      '@media (min-width:600px)': {
        fontSize: 28,
      }
    },
    h3: {
      fontSize: 20,
      '@media (min-width:600px)': {
        fontSize: 24,
      }
    },
    h4: {
      fontSize: 18,
      '@media (min-width:600px)': {
        fontSize: 22,
      }
    },
    h5: {
      fontSize: 16,
      '@media (min-width:600px)': {
        fontSize: 20,
      }
    },
    h6: {
      fontSize: 14,
      '@media (min-width:600px)': {
        fontSize: 18,
      }
    },
    subtitle1: {
      fontSize: 14,
      '@media (min-width:600px)': {
        fontSize: 16,
      }
    },
    subtitle2: {
      fontSize: 12,
      '@media (min-width:600px)': {
        fontSize: 14,
      }
    },
    body1: {
      fontSize: 14,
      '@media (min-width:600px)': {
        fontSize: 16,
      }
    },
    body2: {
      fontSize: 12,
      '@media (min-width:600px)': {
        fontSize: 14,
      }
    },
    button: {
      fontSize: 12,
      '@media (min-width:600px)': {
        fontSize: 14,
      }
    },
    caption: {
      fontSize: 10,
      '@media (min-width:600px)': {
        fontSize: 12,
      }
    },
    overline: {
      fontSize: 8,
      '@media (min-width:600px)': {
        fontSize: 10,
      }
    },
  },
});

export default theme;
