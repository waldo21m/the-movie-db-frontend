// Each reducer have your own state
import {
  MOVIE_LOAD,
  MOVIE_SUCCESS,
  MOVIE_CLEAR_ERROR,
  MOVIE_ERROR,
} from '../types';

/**
 * In the initial state, we defined all the component or container (or view,
 * whatever) states. It's very familiar to useState
 */
const initialState = {
  loading: true,
  movie: {},
  error: false,
  errorMessage: '',
};

/**
 * Everything reduces works with a switch and depends on the payload.
 */
export default function movieReducer(state = initialState, action) {
  switch (action.type) {
    case MOVIE_LOAD:
      return {
        ...state,
        loading: true,
        error: false,
        errorMessage: '',
      };
    case MOVIE_SUCCESS:
      return {
        ...state,
        loading: false,
        movie: action.payload,
      };
    case MOVIE_ERROR:
      return {
        ...state,
        loading: false,
        error: true,
        errorMessage: action.payload,
      };
    case MOVIE_CLEAR_ERROR:
      return {
        ...state,
        error: false,
        errorMessage: '',
      };
    default:
      return state;
  }
};
