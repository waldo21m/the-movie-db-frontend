// Each reducer have your own state
import {
  MOVIES_LIST_LOAD,
  MOVIES_LIST_SUCCESS,
  MOVIES_LIST_CLEAR_ERROR,
  MOVIES_LIST_ERROR,
} from '../types';

/**
 * In the initial state, we defined all the component or container (or view,
 * whatever) states. It's very familiar to useState
 */
const initialState = {
  loading: true,
  moviesList: [],
  error: false,
  errorMessage: '',
};

/**
 * Everything reduces works with a switch and depends on the payload.
 */
export default function movieReducer(state = initialState, action) {
  switch (action.type) {
    case MOVIES_LIST_LOAD:
      return {
        ...state,
        loading: true,
        error: false,
        errorMessage: '',
      };
    case MOVIES_LIST_SUCCESS:
      return {
        ...state,
        loading: false,
        moviesList: action.payload,
      };
    case MOVIES_LIST_ERROR:
      return {
        ...state,
        loading: false,
        error: true,
        errorMessage: action.payload,
      };
    case MOVIES_LIST_CLEAR_ERROR:
      return {
        ...state,
        error: false,
        errorMessage: '',
      };
    default:
      return state;
  }
};
